import { lazy } from "react";

export const routes = {
	GLOBAL: {
		PATH: "/",
		ELEMENT: lazy(() => import("@/pages/home")),
	},
	HOME: {
		PATH: "/home",
		ELEMENT: lazy(() => import("@/pages/home")),
	},
	NOTFOUND: {
		PATH: "*",
		ELEMENT: lazy(() => import("@/shared/Components/NotFound")),
	},
};

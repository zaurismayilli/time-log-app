import { Suspense } from 'react';
import { Route, Routes } from 'react-router-dom';
import Loading from '@/shared/Components/Loading';

import { routes } from './router';

const Root = () => {
  const route = Object.entries(routes).map((item, key) => {
    const { ELEMENT, PATH } = item[1];
    return (
      <Route
        key={key}
        path={PATH}
        element={
          <Suspense fallback={<Loading />} >
            <ELEMENT />
          </Suspense>}
      />
    )
  })

  return <Routes> {route}</Routes>
}

export default Root;

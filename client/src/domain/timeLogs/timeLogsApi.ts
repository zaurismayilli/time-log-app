import { BASE_API } from "@/shared/constant";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const timeLogsApi = createApi({
	baseQuery: fetchBaseQuery({ baseUrl: BASE_API }),
	tagTypes: ["timeLogs"],
	reducerPath: "timeLogs",
	endpoints: (build) => ({
		fetchLogs: build.query({
			query: ({ page, limit }) => ({
				url: `/logs?page=${page}&limit=${limit}`,
				method: "GET",
			}),
			providesTags: ["timeLogs"],
		}),
		addLogs: build.mutation({
			query: (body) => ({
				url: "/logs",
				method: "POST",
				body,
			}),
			invalidatesTags: ["timeLogs"],
		}),
	}),
});

export const timeLogsHooks = {
	useTimeLogs: timeLogsApi.useFetchLogsQuery,
	addTimeLogs: timeLogsApi.useAddLogsMutation,
};

export default timeLogsApi;

import { api as timeLogsApi } from "./timeLogs";

export const reducer = {
	[timeLogsApi.reducerPath]: timeLogsApi.reducer,
};

export const middleware = [timeLogsApi.middleware];

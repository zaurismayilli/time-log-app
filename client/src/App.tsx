import Routes from "./routes";
import Layout from "./shared/layout";

import '@/assets/style/global.scss'

function App() {

  return (
    <Layout>
      <Routes />
    </Layout>
  )
}

export default App

const dateFormatter = {
	inputValue: (date: Date = new Date()) => {
		const local = new Date(date);
		local.setMinutes(local.getMinutes() - local.getTimezoneOffset());
		return local.toJSON().slice(0, 10);
	},
	display: (date: Date = new Date()) => {
		return new Date(date)
			.toJSON()
			.replace(/([\.])+([\dA-Z])*/, "")
			.replace("T", " ");
	},

	returnBack: (date:any = new Date()) => {
		return new Date(date).getTime()
	},
};

export default dateFormatter;

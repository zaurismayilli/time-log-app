import React, { ReactElement } from 'react'
import Header from "@/shared/Components/Header"
import Footer from "@/shared/Components/Footer"

import './index.scss'

const Layout = ({ children }: { children: ReactElement }) => (
  <>
    <Header />
    <div className='body' >{children}</div>
    <Footer />
  </>
)

export default Layout

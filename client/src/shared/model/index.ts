import { ReactNode } from "react";

export interface IUseForm {
	description?: string;
	time_spent?: string;
	created_at?: any;
}

export interface ITimeLogs {
	description: string;
	time_spent: string;
	time_stampt: Date;
}

export interface ITime {
	created_at: Date;
	_id: string;
	logs: ITimeLogs[];
}

export interface ITimeResponse {
	data: { data: ITime };
	code: number;
}

import { Link } from 'react-router-dom'
import logo from '@/assets/icons/logo.gif'

import Typography, { ETypographyVariant } from '../ui/Typography'
import Block, { EBlockMargin } from '../ui/Block'
import FlexBox from '../ui/FlexBox'

import './index.scss'

const Header = () => (
  <header className='header'>
    <FlexBox>
      <Link to="/" className='header--logo' >
        <img src={logo} alt={"time-app"} />
      </Link>
      <Block margin={EBlockMargin.ML3} >
        <Typography variant={ETypographyVariant.H2} >Time App</Typography>
      </Block>
    </FlexBox>
  </header>
)

export default Header

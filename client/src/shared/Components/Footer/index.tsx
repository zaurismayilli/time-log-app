import Paragraph, { EParagraphTheme } from '../ui/Paragraph'

import './index.scss'

const Footer = () => {
  const date = new Date().getFullYear()
  return (
    <footer className='footer' >
      <Paragraph theme={EParagraphTheme.LIGHT} > All right reserved {date} </Paragraph>
    </footer >
  )
}

export default Footer

import React from 'react'
import { Link } from 'react-router-dom'
import logo from '@/assets/icons/logo.gif'
import image from '@/assets/icons/error-logo.gif'
import './index.scss'
import Typography, { ETypographyVariant } from '../ui/Typography'

const NotFound = () => {
  return (
    <div className='not-found'>
      <div className='not-found--image' >
        <img src={image} alt="not-found" />
      </div>
      <Typography variant={ETypographyVariant.H1} >
        O_o Nout found o_O
      </Typography>
    </div>
  )
}

export default NotFound

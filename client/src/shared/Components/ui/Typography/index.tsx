import React from 'react'

import './index.scss'

export enum ETypographyTheme {
  LIGHT = 'light',
  DARK = 'dark'
}

export enum ETypographyVariant {
  H1 = "h1",
  H2 = "h2",
  H3 = "h3",
  H4 = "h4",
  H5 = "h5",
  H6 = "h6",
}

interface IProps {
  className?: string;
  children: any;
  theme?: ETypographyTheme,
  variant: ETypographyVariant
}

const Typography = (
  {
    className,
    children,
    theme = ETypographyTheme.DARK,
    variant = ETypographyVariant.H1
  }
    : IProps) => {
  const Element = variant;
  return (
    <Element className={`typography ${className ? `typography--${className}` : ''} typography--color-${theme} typography--variant-${variant} `} >
      {children}
    </Element>
  )
}

export default Typography

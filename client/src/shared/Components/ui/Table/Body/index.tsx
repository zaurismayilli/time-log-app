import { ReactNode } from 'react';

interface IProps {
  children: ReactNode | ReactNode[];
}

const TableBody = ({ children }: IProps) => (
  <tbody className='table--tbody' >
    {children}
  </tbody>
)

export default TableBody;

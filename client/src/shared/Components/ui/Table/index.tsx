import { ReactNode } from 'react'

import TableBody from './Body';

import './index.scss'

interface IProps {
  children?: ReactNode;
  head: Array<string>
}

const Table = (
  {
    children,
    head
  }
    : IProps) => {

  const THead = head.map((item, key) => <th className='table--thead-tr-th' {...{ key }} >  {item} </th>)

  return (
    <table className='table' >
      <thead className='table--thead' >
        <tr className='table--thead-tr' >
          {THead}
        </tr>
      </thead>
      <TableBody>
        {children}
      </TableBody>
    </table>
  )
}

export default Table

import { ReactNode, FC, HTMLAttributes } from "react";

interface IProps extends HTMLAttributes<HTMLElement> {
  children: ReactNode | ReactNode[];
}

const TableCol: FC<IProps> = ({
  children,
}) => {
  return (
    <td className="table--tbody-tr-td" >
      {children}
    </td>
  );
};

export default TableCol;

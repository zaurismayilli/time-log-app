import { ReactNode, FC, HTMLAttributes } from 'react';

interface IProps extends HTMLAttributes<HTMLElement> {
  children: ReactNode | ReactNode[];
}
const TableRow: FC<IProps> = ({ children }) => <tr className='table--tbody-tr' >{children}</tr>;

export default TableRow;

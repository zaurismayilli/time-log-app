import React, { SelectHTMLAttributes } from 'react'

import './index.scss'

export interface IOption {
  key: number;
  value: string
}

interface IProps extends SelectHTMLAttributes<HTMLSelectElement> {
  options: IOption[],
  defaultValue?: any,
  title?: string;
}

const Select = (
  {
    options,
    defaultValue,
    title = "title",
    ...rest
  }: IProps
) => {
  const option = options.map(({ key, value }) => <option className='select-box--option' key={key} value={key} > {value} </option>)
  return (
    <select {...rest} className='select-box' >
      <option className='select-box--option'  > {title} </option>
      {option}
    </select>
  )
}

export default Select

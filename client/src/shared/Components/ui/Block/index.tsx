import React, { ReactNode } from 'react'

import './index.scss'

export enum EBlockMargin {
  M1 = "margin-5",
  M2 = "margin-10",
  M3 = "margin-15",
  MB1 = "margin-bottom-5",
  MB2 = "margin-bottom-10",
  MB3 = "margin-bottom-15",
  MT1 = "margin-top-5",
  MT2 = "margin-top-10",
  MT3 = "margin-top-15",
  ML1 = "margin-left-5",
  ML2 = "margin-left-10",
  ML3 = "margin-left-15",
  MR1 = "margin-right-5",
  MR2 = "margin-right-10",
  MR3 = "margin-right-15",
}

export enum EBlockPadding {
  P1 = "padding-5",
  P2 = "padding-10",
  P3 = "padding-15",
  PB1 = "padding-bottom-5",
  PB2 = "padding-bottom-10",
  PB3 = "padding-bottom-15",
  PT1 = "padding-top-5",
  PT2 = "padding-top-10",
  PT3 = "padding-top-15",
  PL1 = "padding-left-5",
  PL2 = "padding-left-10",
  PL3 = "padding-left-15",
  PR1 = "padding-right-5",
  PR2 = "padding-right-10",
  PR3 = "padding-right-15",
}

interface IProps {
  children: ReactNode | ReactNode[];
  margin?: EBlockMargin;
  padding?: EBlockPadding;
  width?: string;
}

const Block = (
  {
    children,
    margin = EBlockMargin.M1,
    padding = EBlockPadding.P1,
    width = '100%'
  }: IProps
) => {
  return (
    <div style={{ width }} className={`block block--${margin} block--${padding}`} >
      {children}
    </div>
  )
}

export default Block

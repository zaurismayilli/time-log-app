import React, { ReactNode } from 'react'

import './index.scss'

export enum EParagraphTheme {
  LIGHT = 'light',
  DARK = 'dark',
  DANGER = 'danger'
}

interface IProps {
  className?: string;
  children: ReactNode;
  theme?: EParagraphTheme
}

const Paragraph = (
  {
    className,
    children,
    theme = EParagraphTheme.DARK
  }
    : IProps) => {
  return (
    <p className={`paragraph ${className ? `paragraph--${className}` : ''} paragraph--color-${theme}`} >
      {children}
    </p>
  )
}

export default Paragraph

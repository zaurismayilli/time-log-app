import { ReactNode } from 'react'

import './index.scss'

export enum EFlexJustify {
  SPACE_AROUND = "space-around",
  SPACE_BETWEEN = "space-between",
  SPACE_END = "space-end",
  CENTER = "center",
  FLEX_START = "flex-start",
  FLEX_END = "flex-end",
}

export enum EFlexAlign {
  BASELINE = "baseline",
  START = "start",
  CENTER = "center",
  FLEX_START = "flex-start",
  FLEX_END = "flex-end",
}

export enum EFlexDirection {
  ROW = "row",
  ROW_REVERSE = "row-reverse",
  COLUMN = "column",
  COLUMN_REVERSE = "cr",
}

interface IProps {
  children: ReactNode | ReactNode[];
  alignItems?: EFlexAlign;
  justifyContent?: EFlexJustify;
  flexDirection?: EFlexDirection;
}

const FlexBox = (
  {
    children,
    alignItems = EFlexAlign.CENTER,
    justifyContent = EFlexJustify.FLEX_START,
    flexDirection = EFlexDirection.ROW,
  }: IProps
) => {
  return (
    <div className={`flexbox flexbox--align-${alignItems} flexbox--justify-${justifyContent} flexbox--direction-${flexDirection}`} >
      {children}
    </div>
  )
}

export default FlexBox

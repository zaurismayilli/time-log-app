import React, { InputHTMLAttributes } from 'react'

import './index.scss'
import FlexBox, { EFlexAlign, EFlexDirection, EFlexJustify } from '../FlexBox';

export enum EInputType {
  TEXT = 'text',
  TIME = 'time',
  DATE = 'date'
}

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string,
  name: string;
  value: any;
  placeholder: string;
  type?: EInputType;
  error?: boolean;
  errorTxt?: string;
}

const Input = (
  {
    label,
    name,
    value,
    placeholder,
    type = EInputType.TEXT,
    error = false,
    errorTxt = 'Please filled out',
    ...rest
  }: IProps
) => {
  return (
    <div className='form-group' >
      <label className='form-group--label' > {label} </label>
      <FlexBox alignItems={EFlexAlign.FLEX_START} flexDirection={EFlexDirection.COLUMN} >
        <input className='form-group--input' {...{ name, placeholder, type, value }} {...rest} />
        {error && !value && <span className='form-group--error' > {errorTxt}: {name} </span>}
      </FlexBox>
    </div>
  )
}

export default Input

import React, { InputHTMLAttributes } from 'react'

import './index.scss'

export enum EButtonType {
  SUBMIT = 'submit',
  BUTTON = 'button',
  RESET = 'reset'
}

export enum EButtonTheme {
  LIGHT = 'light',
  DARK = 'dark',
  PRIMARY = 'primary'
}

interface IProps extends InputHTMLAttributes<HTMLInputElement> {
  title: string,
  theme?: string;
  type?: EButtonType;
}

const Button = (
  {
    title,
    theme = EButtonTheme.PRIMARY,
    type = EButtonType.BUTTON,
    ...rest
  }: IProps
) => {
  return (
    <button {...{type}} className={`button button--color-${theme}`} >{title}</button>
  )
}

export default Button

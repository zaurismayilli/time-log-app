import FlexBox, { EFlexAlign, EFlexJustify } from '@/shared/Components/ui/FlexBox'
import Button, { EButtonType } from '@/shared/Components/ui/Button'
import Input, { EInputType } from '@/shared/Components/ui/Input'
import Table from '@/shared/Components/ui/Table'
import TableCol from '@/shared/Components/ui/Table/Col'
import TableRow from '@/shared/Components/ui/Table/Row'
import Typography, { ETypographyVariant } from '@/shared/Components/ui/Typography'
import React, { Suspense, useState } from 'react'
import Block, { EBlockMargin, EBlockPadding } from '@/shared/Components/ui/Block'
import dateFormatter from '@/shared/utils/date'
import { timeLogsHooks } from '@/domain/timeLogs/timeLogsApi'
import { IUseForm, ITime, ITimeLogs } from '@/shared/model'
import Loading from '@/shared/Components/Loading'
import Pagination from '@/shared/Components/Pagination'
import { useNavigate, useSearchParams } from 'react-router-dom'
import Paragraph, { EParagraphTheme } from '@/shared/Components/ui/Paragraph'
import Select, { IOption } from '@/shared/Components/ui/Select'

const tableHead = ["Description", "Time spent", "Date"]

const limitOptions: IOption[] = [
  {
    key: 3,
    value: "3"
  },
  {
    key: 5,
    value: "5"
  },
  {
    key: 7,
    value: "7"
  }
]

const Home = () => {
  const [useForm, setUseForm] = useState<IUseForm>(
    {
      description: '',
      time_spent: '',
      created_at: dateFormatter.inputValue(),
    }
  );
  const { description, time_spent, created_at } = useForm;
  const [searchParams, setSearchParams] = useSearchParams();
  const navigate = useNavigate();

  const page = searchParams.get('page') ?? 1;
  const limit = searchParams.get('limit') ?? 3;

  const [addLogs] = timeLogsHooks.addTimeLogs()

  const { data: logs, isLoading, isError } = timeLogsHooks.useTimeLogs({ page, limit })

  const inputArr = [
    {
      label: "Description",
      name: "description",
      placeholder: "Description",
      required: true,
      value: description,
      onChange: (e: React.ChangeEvent<HTMLInputElement>) => setUseForm({ ...useForm, description: e.target.value }),
    },
    {
      label: "Time Spent",
      name: "time_spent",
      placeholder: "Time Spent",
      type: EInputType.TEXT,
      required: true,
      value: time_spent,
      onChange: (e: React.ChangeEvent<HTMLInputElement>) => setUseForm({ ...useForm, time_spent: e.target.value }),
    },
    {
      label: "Created At",
      name: "created_at",
      placeholder: "Created at",
      value: created_at,
      type: EInputType.DATE,
      onChange: (e: React.ChangeEvent<HTMLInputElement>) => setUseForm({ ...useForm, created_at: e.target.value }),
    },
  ]

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!time_spent || !description) return false
    addLogs({
      description,
      time_spent,
      created_at: dateFormatter.returnBack(created_at)
    })
    navigate('/');
    return setUseForm({
      description: '',
      time_spent: '',
      created_at: dateFormatter.inputValue(created_at)
    })
  }

  if (isLoading) return <Loading />

  if(isError) return  <Paragraph theme={EParagraphTheme.DANGER} > Someting get wrong ... </Paragraph>

  const inputs = inputArr.map(attr => <Input {...attr} key={attr.name} />)

  const tableCol = (logs: ITimeLogs[]) => logs.map((
    {
      description,
      time_spent,
      time_stampt
    },
    key: number
  ) => (
    <TableRow {...{ key }} >
      <TableCol> {description} </TableCol>
      <TableCol> {time_spent} </TableCol>
      <TableCol> {dateFormatter.display(time_stampt)} </TableCol>
    </TableRow>
  ))

  const table = logs.data.map(({ created_at, logs }: ITime, key: number) => (
    <>
      <Block margin={EBlockMargin.MB1} >
        <Typography variant={ETypographyVariant.H2} > {dateFormatter.inputValue(created_at)} </Typography>
      </Block>
      <Table head={tableHead} >
        {tableCol(logs)}
      </Table>
    </>
  ))

  const handleLimit = (e: React.ChangeEvent<HTMLSelectElement>) => {
    searchParams.set("limit", e.target.value)
    setSearchParams(searchParams)
  }

  return (
    <FlexBox justifyContent={EFlexJustify.SPACE_AROUND} alignItems={EFlexAlign.FLEX_START} >
      <Block width="65%" padding={EBlockPadding.PT3}>
        <Block margin={EBlockMargin.MB3} >
          <Typography variant={ETypographyVariant.H1} > My Time Log </Typography>
        </Block>
        {table.length ? table : <Paragraph theme={EParagraphTheme.DANGER} > No any Logs </Paragraph>}
        <FlexBox justifyContent={EFlexJustify.SPACE_BETWEEN} >
          <Pagination count={logs?.count} />
          <Select onChange={handleLimit} title={"Select limit"} options={limitOptions} />
        </FlexBox>
      </Block>
      <Block width="30%" padding={EBlockPadding.PL3}>
        <form onSubmit={onSubmit} style={{ marginTop: '85px' }} >
          {inputs}
          <FlexBox justifyContent={EFlexJustify.FLEX_END} >
            <Button type={EButtonType.SUBMIT} title='submit' />
          </FlexBox>
        </form>
      </Block>
    </FlexBox>
  )
}

export default Home

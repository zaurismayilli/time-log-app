export const toDay = (date: Date = new Date()) => {
	const startDay = new Date(date);

	startDay.setHours(0, 0, 0, 0);

	const endDay = new Date(date);
	return {
		startDay: startDay.getTime(),
		endDay: endDay.getTime(),
	};
};

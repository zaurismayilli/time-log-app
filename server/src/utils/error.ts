import { Response } from "express";

interface IResponse {
	code?: number;
	data: any;
	res: Response;
}

export const NewError = ({ code = 400, data, res }: IResponse) => {
	return res.status(code).json(data);
};

export const NewSuccess = ({ code = 200, data, res }: IResponse) => {
	return res.status(code).json({
		data,
		count:data.length,
		code,
	});
};

import Server from "./server";
import apiRouter from "./routes";
import dbClient from "./db";
import TimeLogStore from "./db/timeLog";
import storeMiddleware from "./middlewares/store";

class TimeAppServer extends Server {
	constructor(port: string | number) {
		super();
		this.PORT = port;
		this.init();
		this.getReady();
	}

	private async getReady() {
		await this.connectDB();
		await this.createRoutes();
	}

	private async connectDB(): Promise<void> {
		try {
			const { DBUSER, DBPASS, DBNAME } = process.env;
			const db = await dbClient({ user: DBUSER ?? "", pass: DBPASS ?? "", name: DBNAME ?? "" });
			db.connect();
			const store = new TimeLogStore(db);
			this.app.use(storeMiddleware(store));
			console.log("CONNECTED TO DB");
		} catch (err) {
			console.log("ERROR ON DB CONNECTION", err);
		}
	}

	private async createRoutes(): Promise<void> {
		this.app.use("/api", apiRouter);
	}
}

export default TimeAppServer;

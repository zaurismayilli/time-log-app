import TimeAppServer from "./time-app-server";

const app = new TimeAppServer(2021);

app.start((port) => {
	console.log("Message Server is listening", port);
});

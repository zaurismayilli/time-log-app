import { Collection, MongoClient } from "mongodb";

import { TIMELOG_COLLECTION } from "./const";
import { toDay } from "../utils/date";

class TimeLogStore {
	private collection: Collection<any>;

	constructor(client: MongoClient) {
		this.collection = client.db().collection(TIMELOG_COLLECTION);
	}

	async creatandupdate(data: any) {
		// return await this.collection.deleteMany({})
		const isUpdate = await this.collection.findOne({
			created_at: { $gte: toDay(data.created_at).startDay, $lt: toDay(data.created_at).endDay + 1 },
		});

		if (!!isUpdate) {
			await this.collection.updateOne(
				{
					created_at: {
						$gte: toDay(data.created_at).startDay,
						$lt: toDay(data.created_at).endDay + 1,
					},
				},
				{ $push: { logs: data.logs[0] } }
			);

			return {
				...data.logs[0],
			};
		}

		return await this.collection.insertOne(data);
	}

	async getList({ limit = 4, page = 1 } = {}) {
		limit = +limit;
		page = +page;
		return await this.collection
			.find({})
			.sort({ created_at: -1 })
			.skip(page * limit - limit)
			.limit(limit)
			.toArray();
	}
}

export default TimeLogStore;

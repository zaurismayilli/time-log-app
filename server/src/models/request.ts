import { Request } from "express";
import TimeLogStore from "../db/timeLog";

export interface IRequest extends Request {
	store: TimeLogStore;
}

export enum EInputType {
  DATE = 'date',
  TEXT = 'text',
}

export interface IInputItem {
  label: string;
  id: string;
  required: boolean;
  placeholder:string;
  type: EInputType;
}
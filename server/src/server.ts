import cors from "cors";
import * as dotenv from "dotenv";
const path = require("path");
import express, { Application, NextFunction, Request, Response, application } from "express";
import { createServer, Server as HTTPServer } from "http";

class Server {
	public env = {};
	public app: Application = application;
	private readonly DEFAULT_PORT: number = 8081;
	public PORT: string | number = this.DEFAULT_PORT;
	private httpServer: HTTPServer = createServer({});

	public init(): void {
		this.app = express();
		this.env = dotenv.config({ path: path.resolve(__dirname, "./.env") });
		this.httpServer = createServer(this.app);
		this.configure();
	}

	private configure(): void {
		this.app.use(cors());
		this.app.use(express.json());
		this.app.use((err: any, req: Request, res: Response, next: NextFunction): void => {
			
			console.log("Uncatched error", err);
			next();
		});
	}

	public start(callback: (port: string | number) => void): void {
		const PORT: string | number = this.PORT || this.DEFAULT_PORT;
		this.httpServer.listen(PORT, () => callback(PORT));
	}
}

export default Server;

import { Request, Response, NextFunction } from "express";

import { timeLogInputs } from "../constant/dummy";
import { NewError } from "../utils/error";
import { COMMON_ERROR } from "../constant/error";

export const createValidation = async (req: Request, res: Response, next: NextFunction) => {
	try {
		const { body } = req;
		const isValid = timeLogInputs.filter((input) => input.required).every(({ id }) => !!body[id]);
		if (!isValid) return NewError({ data: COMMON_ERROR.missingFields, res });
		next();
	} catch (err) {
		return NewError({ data: err, res });
	}
};

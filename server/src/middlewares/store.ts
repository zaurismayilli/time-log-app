import { NextFunction, Response } from "express";
import TimeLogStore from "../db/timeLog";
import { IRequest } from "../models/request";

const storeMiddleware =
	(store: TimeLogStore) =>
	(req: any, res: Response, next: NextFunction): void => {
		if (store) req.store = store;
		next();
	};

export default storeMiddleware;

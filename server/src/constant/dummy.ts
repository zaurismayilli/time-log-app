import { EInputType, IInputItem } from "../models/form";

export const timeLogInputs: IInputItem[] = [
	{
		label: "Description",
		required: true,
		placeholder: "Description",
		id: "description",
		type: EInputType.TEXT,
	},
  {
		label: "Time spent",
		required: true,
		placeholder: "Time spent",
		id: "time_spent",
		type: EInputType.TEXT,
	},
];

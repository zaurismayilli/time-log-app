import { Response } from "express";
import { NewError, NewSuccess } from "../utils/error";

export const createController = async (req: any, res: Response) => {
	try {
		const { body } = req;

		const date = new Date().getTime();
		body.time_stampt = date;
		const created_at = body.created_at || date;

		delete body.created_at;

		const response = {
			created_at,
			logs: [body],
		};
		const data = await req.store.creatandupdate(response);

		return NewSuccess({ data, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};

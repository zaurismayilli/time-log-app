import { Router } from "express";

import { getListController } from "./read";
import { createController } from "./create";
import { createValidation } from "../middlewares/validation";

const router = Router();

router.post("/logs", createValidation, createController);
router.get("/logs", getListController);

export default router;

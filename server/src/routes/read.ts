import { Response } from "express";

import { NewSuccess, NewError } from "../utils/error";

export const getListController = async (req: any, res: Response) => {
	const { limit, page } = req?.query;

	try {
		const logs = await req.store.getList({ limit, page });

		return NewSuccess({ data: logs, res });
	} catch (err) {
		return NewError({ data: err, res });
	}
};
